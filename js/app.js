// File: js/app.js
// NOTE: EL ARCHIVO APP ES EL PRINCIPAL DE LA APLICACION EN EL CUAL SE CARGAN LAS RUTAS Y LOS MODULOS DE TODA LA APLICACION

// NOTE: Use stric es para usar javascript en un modo estricto
(function() {
    'use strict';

    // NOTE: Instanciamos una variable con el nombre clinicaApp que contiene el modulo de angular de la aplicacion
    var clinicaApp = angular.module('clinicaApp', [
        'ui.router',
        'homeModule',
        'PanelModule',
        'CitasModule',
        'CitaModule',
        'citaUsuarioModule',
        'ControlModule',
        'AppServices',
    ]);

    // NOTE: Son las configuraciones generales del modulo principal de la aplicacion
    clinicaApp.config(function($stateProvider, $urlRouterProvider) {

        // NOTE: Creamos una ruta por defecto para las rutas si entramos a la aplicacion por default nos llevara a la ruta HOME
        $urlRouterProvider.otherwise("/home");

        $stateProvider
        // NOTE: Agregamos un nuevo estado a la aplicacion 'home'
        .state('home', {
            // NOTE: Se agrega la url de la ruta
            url: '/home',
            // NOTE: Agregamos la vista donde se ira cargando la aplicacion en este caso es main
            views: {
                "main": {
                    // NOTE: Agregamos la template que se cargara en la vista
                    templateUrl: 'partials/home/home.html',
                    // NOTE: Agregamos el controlador de la vista
                    controller: 'homeCtrl',
                }
            }
        })

        .state('panel', {
            url: '/panel',
            private: true,
            views: {
                "main": {
                    templateUrl: 'partials/panel/inicio.html',
                    controller: 'panelCtrl',
                }
            }
        })

        .state('citas', {
            url: '/citas',
            private: true,
            views: {
                "main": {
                    templateUrl: 'partials/citas/inicio.html',
                    controller: 'citasCtrl',
                }
            }
        })

        .state('cita', {
            url: '/citas/:id',
            private: true,
            views: {
                "main": {
                    templateUrl: 'partials/citas/detalles.html',
                    controller: 'citaCtrl',
                }
            }
        })

        .state('cita_usuario', {
            url: '/cita_usuario/:user',
            private: true,
            views: {
                "main": {
                    templateUrl: 'partials/panel/usuario.html',
                    controller: 'citaUsuarioCtrl',
                }
            }
        })

        .state('control_cita', {
            url: '/control_cita/:cita/:expediente',
            private: true,
            views: {
                "main": {
                    templateUrl: 'partials/panel/control.html',
                    controller: 'controlCtrl'
                }
            }
        });
    });

    clinicaApp.run(function($rootScope, $location) {
        $rootScope.$on('$stateChangeStart', function(event, toState) {
          if(toState.private){
            if(sessionStorage.logged != 'true'){
              $location.path('/home');
            }
          }
        });
    });
})();
