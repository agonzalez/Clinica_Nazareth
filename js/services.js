// NOTE: Los sevicios del sistema que son nuestra coneccion con las vistas del backend
(function() {
    'use strict';

    /* Services */
    var AppServices = angular.module('AppServices', ['ngResource']);
    // NOTE: Servicio de login
    AppServices.factory('loginService', ['$http', '$state',
        function($http, $state){
            return{
                // NOTE: funcion de login que es la encargada de poder hacer login
                login:function(user){
                    // NOTE: crea una varibale con los parametros a enviar en la url
                    var data = '&user=' + user.user + '&password=' + user.password;

                    // NOTE: Hacemos una peticion porst a la url de login de la vista de PHP
                    $http.post('views/login.php?task=login' + data).
                    success(function(data, status) {
                        if (data.status == true){
                            // NOTE: en el success agregamos en session el usuario y el logged para qe se atru
                          sessionStorage.user = data.user;
                          sessionStorage.logged = 'true';
                          $('.modal-backdrop').remove();
                          // NOTE: hacemos una redireccion al panel una vez que haya sido logueado
                          $state.go('panel');
                        }else{
                            // NOTE: Datos incorrectos hace un alert
                          alert('Tu contrasena o correo son incorrectos.');
                        }
                    }).error(function(data, status) {
                        // NOTE: Datos incorrectos hace un alert
                        alert('Ah ocurrido un erro intentalo mas tarde.');
                    });

                },

            };
          }
    ]);

    // NOTE: Servicio usuario
    AppServices.factory('userService', ['$http', '$state',
        function($http, $state){
            return{

                getUser:function(user_id, callback){
                  $http.post('views/users.php?task=getOne&id=' + user_id).then(function(data){
                    callback(data.data);
                  });
                },

                getUsers:function(callback){
                  $http.post('views/users.php?task=getall').then(function(data){
                    callback(data.data);
                  });
                },

                insertUser: function(data, callback) {

                    var postdata = '&nom1_usu=' + data.nom1_usu + '&ape1_usu=' + data.ape1_usu + '&fec_nac_usu=' + data.fec_nac_usu + '&gen_usu=' + data.gen_usu + '&con_usu=' + data.con_usu + '&fec_ins_usu=' + data.fec_ins_usu + '&usu_usu=' + data.usu_usu + '&cod_esp_rol=' + data.cod_esp_rol + '&cor_usu=' + data.cor_usu;

                    $http.post('views/users.php?task=insert' + postdata).then(function(data){
                        alert('Ya estas registrado :)')
                    })
                }

            };
          }
    ]);

    // NOTE: Servicio de login
    AppServices.factory('tipoConsultaService', ['$http', '$state',
        function($http, $state){
            return{

                getAll:function(callback){
                  $http.post('views/tipo_consulta.php?task=getall').then(function(data){
                    callback(data.data);
                  });
                }
            };
          }
    ]);

    // NOTE: Servicio de login
    AppServices.factory('TipoTratamientoService', ['$http', '$state',
        function($http, $state){
            return{

                getAll:function(callback){
                  $http.post('views/tipo_tratamiento.php?task=getall').then(function(data){
                    callback(data.data);
                  });
                }
            };
          }
    ]);



    AppServices.factory('HistorialService', ['$http',
        function($http){
            return{

                insert:function(historial, callback){

                  var dataset = '&tem_him=' + historial.tem_him + '&pes_him=' + historial.pes_him + '&ten_him=' + historial.ten_him + '&pul_him=' + historial.pul_him + '&can_exa=' + historial.can_exa + '&cod_tic=' + historial.cod_tic + '&cod_cit=' + historial.cod_cit + '&cod_exp=' + historial.cod_exp;

                  $http.post('views/historial_medico.php?task=insert' + dataset).then(function(data){
                    callback(data.data);
                  });
                },

            };
          }
    ]);

    AppServices.factory('FacturasService', ['$http',
        function($http){
            return{

                insert:function(factura, callback){

                  var dataset = '&mon_med_fac=' + factura.mon_med_fac + '&cod_tic=' + factura.cod_tic + '&cod_usu=' + factura.cod_usu + '&cod_tip=' + factura.cod_tip

                  $http.post('views/facturas.php?task=insert' + dataset).then(function(data){
                    callback(data.data);
                  });
                },

            };
          }
    ]);

    AppServices.factory('TratamientoService', ['$http',
        function($http){
            return{

                insert:function(tratamiento, callback){

                  var dataset = '&fec_ini_tra=' + tratamiento.fec_ini_tra + '&obs_tra=' + tratamiento.obs_tra + '&cod_him=' + tratamiento.cod_him + '&cod_tit=' + tratamiento.cod_tit;

                  $http.post('views/tratamiento.php?task=insert' + dataset).then(function(data){
                    callback(data.data);
                  });
                },

                getOne:function(id, callback){
                  $http.post('views/tratamiento.php?task=getOne&id=' + id).then(function(data){
                    callback(data.data);
                  });
                },

            };
          }
    ]);

    // NOTE: Servicio de login
    AppServices.factory('CitasService', ['$http', '$location',
        function($http, $location){
            return{

                insertDate:function(date){

                  var dataset = '&fin_cit=' + '0' + '&mot_cit=' + date.mot_cit + '&est_cit=' + date.est_cit + '&cod_usu=' + date.cod_usu + '&ini_cit=' + date.ini_cit;

                  $http.post('views/citas.php?task=insert' + dataset).then(function(data){
                    window.location.reload();
                  });
                },

                validateDate: function(cita, callback) {
                    $http.post('views/citas.php?task=validate&date=' + cita.ini_cit).then(function(data){
                      callback(data.data);
                    });
                },

                update: function(cita, callback) {

                    var data = '&id=' + cita.cod_cit + '&cod_cit=' + cita.cod_cit + '&ini_cit=' + cita.ini_cit + '&mot_cit=' + cita.mot_cit + '&est_cit=' + cita.est_cit +  '&cod_usu=' + cita.cod_usu + '&fin_cit=' + 'ahora';

                    $http.post('views/citas.php?task=update' + data).then(function(data){
                      callback(data.data);
                    });
                },

                getOne:function(cita_id, callback){
                  $http.post('views/citas.php?task=getOne&id=' + cita_id).then(function(data){
                    callback(data.data);
                  });
                },

                getAll:function(callback) {
                    $http.post('views/citas.php?task=getall').then(function(data){
                      callback(data.data);
                    });
                }

            };
          }
    ]);




})();
