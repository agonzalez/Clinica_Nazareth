(function() {
    'use strict';

    angular.module('CitasModule', []);

    var CitasModule = angular.module('CitasModule');

    CitasModule.controller('citasCtrl', ['$scope', 'userService', 'CitasService', '$state', '$filter',
        function($scope, Users, Citas, $state, $filter) {

            $scope.back = function() {
                window.history.back();
            };

        $scope.horario = [
            '8:00 AM',
            '8:30 AM',
            '9:00 AM',
            '9:30 AM',
            '10:00 AM',
            '10:30 AM',
            '11:00 AM',
            '11:30 AM',
            '1:00 PM',
            '1:30 PM',
            '2:00 PM',
            '2:30 PM',
            '3:00 PM',
            '3:30 PM',
        ];

        $scope.dateToday = new Date();

        $scope.filterDates = 'Pendiente';

        $scope.estado = function(dato) {
            $scope.filterDates = dato;
        };

        console.log($scope.dateToday);
          $scope.id_user = sessionStorage.user;

           Users.getUser(sessionStorage.user, function (response) {
            $scope.user = response;
          });

          Users.getUsers(function(response) {
              $scope.users = response;
          });

          $scope.citaDetails = function(id) {
              console.log(id);
          };

          $scope.logout = function() {
              sessionStorage.clear();
              $state.go('home');
          };

          $scope.cancelar = function(cita) {
              cita.est_cit = 'Cancelada';
              Citas.update(cita, function(response) {
                  cita = response;
              });
          };

          $scope.sendDate = function(cita) {


            cita.cod_usu = sessionStorage.user;
            cita.est_cit = 'Pendiente';
            cita.ini_cit = $filter('date')(cita.dia, "yyyy-MM-dd")+cita.hora;

            if ($filter('date')(cita.dia, "EEEE") == 'Sunday'){
                alert('No hay citas los dias domingos');
            }else{
                Citas.validateDate(cita, function(data) {
                    if (data == 1) {
                        alert('Horario de cita no disponible');
                    }else{
                        Citas.insertDate(cita);
                    }
                });
            }

          };

          $scope.setUser = function(user_id) {
              $state.go('cita_usuario', {
                  'user':user_id
              });
          };



        }
    ]);
})();
