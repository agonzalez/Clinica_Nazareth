(function() {
    'use strict';

    angular.module('citaUsuarioModule', []);

    var citaUsuarioModule = angular.module('citaUsuarioModule');

    citaUsuarioModule.controller('citaUsuarioCtrl', ['$scope', 'userService', 'CitasService', '$state', '$stateParams',
        function($scope, Users, Citas, $state, $stateParams) {

            $scope.back = function() {
                window.history.back();
            };

            $scope.logout = function() {
                sessionStorage.clear();
                $state.go('home');
            };

          $scope.id_user = sessionStorage.user;

          Users.getUser($stateParams.user, function (response) {
            $scope.user = response;
          });

          $scope.selectDate = function(cita) {
              $state.go('control_cita', {
                  'cita': cita.cod_cit
              });
          };

        }
    ]);
})();
