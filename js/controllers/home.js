(function() {
    'use strict';

    angular.module('homeModule', []);

    var homeModule = angular.module('homeModule');

    homeModule.controller('homeCtrl', ['$scope', 'loginService', 'userService', 'CitasService', '$filter',
        function($scope, Login, User, Citas, $filter) {

            $scope.dateToday = new Date();

            $scope.horario = [
                '8:00 AM',
                '8:30 AM',
                '9:00 AM',
                '9:30 AM',
                '10:00 AM',
                '10:30 AM',
                '11:00 AM',
                '11:30 AM',
                '1:00 PM',
                '1:30 PM',
                '2:00 PM',
                '2:30 PM',
                '3:00 PM',
                '3:30 PM',
            ];

            $scope.accept = function() {
                $scope.user.password = window.btoa($scope.user.password);
                Login.login($scope.user);
            };

            $scope.registUser = function(data) {
                data.cod_esp_rol = 1;
                data.fec_ins_usu = new Date();
                data.con_usu = window.btoa(data.con_usu);
                User.insertUser(data);
            };

            $scope.sendDate = function() {
              $scope.cita = {};

              $scope.cita.ini_cit = $filter('date')($scope.dia, "yyyy-MM-dd") + $scope.hora;

              Citas.validateDate($scope.cita, function(data) {
                  if (data == 1) {
                      $scope.color_message = 'red';
                      $scope.message = 'Horario de cita no disponible';
                  }else{
                      $scope.color_message = 'green';
                      $scope.message = 'Horario disponible inicia sesion para poder reservar tu cita';
                  }
              });

            };

        }
    ]);
})();
