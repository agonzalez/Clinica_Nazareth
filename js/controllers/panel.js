(function() {
    'use strict';

    angular.module('PanelModule', []);

    var PanelModule = angular.module('PanelModule');

    PanelModule.controller('panelCtrl', ['$scope', 'userService', 'CitasService', '$state', 'FacturasService',
        function($scope, Users, Citas, $state, Facturas) {
            $scope.back = function() {
                window.history.back();
            };
            $scope.dateToday = new Date();

          $scope.id_user = sessionStorage.user;

          Users.getUser(sessionStorage.user, function (response) {
            $scope.user = response;
          });

          Users.getUsers(function(response) {
              $scope.users = response;
          });

          Citas.getAll(function(response) {
              $scope.citas = response;
          });

          $scope.aprobar = function(cita) {
              cita.est_cit = 'Aprobada';
              Citas.update(cita, function(response) {
                  cita = response;
              });
          };

          $scope.cancelar = function(cita) {
              cita.est_cit = 'Cancelada';
              Citas.update(cita, function(response) {
                  cita = response;
              });
          };

          $scope.costo = function(id) {
              Citas.getOne(id, function(data) {
                   $scope.currentDate = data;
                   if($scope.currentDate.cita.est_cit == 'Pagada'){
                       $scope.currentDate.isChecked = 'hide';
                   }
                   $('#FacurasModal').modal();
              });
          };
          $scope.filterDates = 'Pendiente';
          $scope.estado = function(dato) {
              $scope.filterDates = dato;
          };

          $scope.payment = function(cita) {

              var cita = cita;

              var data = {
                  'mon_med_fac': cita.costo.cos_tic,
                  'cod_tic': cita.costo.cod_tic,
                  'cod_usu': cita.cita.cod_usu,
                  'cod_tip': 1
              };

              Facturas.insert(data, function(nais) {
                  cita.cita.est_cit = 'Pagada';
                  Citas.update(cita.cita, function(response) {
                      cita = response;
                  });
              });
          };

          $scope.sendDate = function(cita) {


            cita.cod_usu = sessionStorage.user;
            cita.est_cit = 'Pendiente';
            cita.ini_cit = $filter('date')(cita.dia, "yyyy-MM-dd")+cita.hora;

            if ($filter('date')(cita.dia, "EEEE") == 'Sunday'){
                alert('No hay citas los dias domingos');
            }else{
                Citas.validateDate(cita, function(data) {
                    if (data == 1) {
                        alert('Horario de cita no disponible');
                    }else{
                        Citas.insertDate(cita);
                    }
                });
            }

          };

          $scope.logout = function() {
              sessionStorage.clear();
              $state.go('home');
              location.reload();
          };

          $scope.sendMail = function(cita) {

            Users.getUser(cita.cod_usu, function (usuario) {

               var datos = 'correo=' + usuario.usuario.cor_usu + '&cita=' + cita.mot_cit + '&fecha=' + cita.ini_cit + '&nombre=' + usuario.usuario.nom1_usu + '&apellido=' + usuario.usuario.ape1_usu;

         		var datastring = datos + '&modo=contacto';

         		$.ajax
           		({
         			type: "POST",
         			url: "views/mail.php",
         			data: datastring,
         			success: function(data)
         			{
         				alert('se ah enviado un correo de confirmacion');
         			},
         			error: function()
         			{
         				alert('Sucedio un error intentalo mas tarde');
         			}
           		});

            });
          }

        }
    ]);
})();
