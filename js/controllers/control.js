(function() {
    'use strict';

    angular.module('ControlModule', []);

    var ControlModule = angular.module('ControlModule');

    ControlModule.controller('controlCtrl', ['$scope', '$state', '$stateParams', 'tipoConsultaService', 'HistorialService', 'CitasService', 'TipoTratamientoService', 'TratamientoService',
        function($scope, $state, $stateParams,TiposConsultas, Historial, Citas, TiposTratamientos, Tratamientos) {

            $scope.back = function() {
                window.history.back();
            };

            TiposConsultas.getAll(function(data){
                $scope.tipos = data
            });

            Citas.getOne($stateParams.cita, function(data) {
                if($.isEmptyObject(data.historial)){
                    $scope.history = true;
                }else{
                    $scope.historial_db = data.historial;
                    Tratamientos.getOne(data.historial.cod_him, function (data) {
                            if($.isEmptyObject(data.tratamiento)){

                               TiposTratamientos.getAll(function(data){
                                   $scope.tipo_tratamientos = data;
                               });

                               $scope.history = false;

                            }else{
                                $scope.history = 'exams';
                            }
                    })
                }
            });

            $scope.add_historial = function(historial) {
                historial.cod_cit = $stateParams.cita;
                historial.cod_exp = $stateParams.expediente;
                Historial.insert(historial, function(data){
                    $scope.history = false
                })
            };

            $scope.add_tratamiento = function(tratamiento) {
                tratamiento.cod_him = $scope.historial_db.cod_him;

                Tratamientos.insert(tratamiento, function(data) {
                    $scope.history = 'exams';
                })
            };

            $scope.logout = function() {
                sessionStorage.clear();
                $state.go('home');
            };

        }
    ]);
})();
