(function() {
    'use strict';

    angular.module('CitaModule', []);

    var CitaModule = angular.module('CitaModule');

    CitaModule.controller('citaCtrl', ['$scope', 'userService', 'CitasService', '$state', '$stateParams',
        function($scope, Users, Citas, $state, $stateParams) {

            $scope.back = function() {
                window.history.back();
            };

          $scope.id_user = sessionStorage.user;

          Citas.getOne($stateParams.id, function(data) {
              $scope.cita = data;
          });

          $scope.logout = function() {
              sessionStorage.clear();
              $state.go('home');
          };
        }
    ]);
})();
