<?php
require_once '../moondragon4.0/moondragon.database.php';

// NOTE: Importa la coneccion de la base de datos
include '../connection/conexion.php';
// NOTE: Declaracion de los modelos

// NOTE: Variaable $config que es un array vacio
$config = array();
// NOTE: Se agrega el nombre de la tabla
$config['table'] = 'cita';
// NOTE: Se agrega la primary key
$config['primary'] = 'cod_cit';
// NOTE: Se agregan los fields de los elementos de la tabla
$config['fields'] = array('ini_cit', 'fin_cit', 'mot_cit', 'est_cit', 'cod_usu');
// NOTE: Se agrega un modelo con el arreglo de la tabla
ModelLoader::addModel('cita', $config);

$config = array();
$config['table'] = 'departamento';
$config['primary'] = 'cod_dep';
$config['fields'] = array('nom_dep');
ModelLoader::addModel('departamento', $config);

$config = array();
$config['table'] = 'dirrecion';
$config['primary'] = 'cod_dir';
$config['fields'] = array('col_dir', 'cal_dir', 'pol_dir', 'dom_dir', 'cod_mun', 'cod_usu');
ModelLoader::addModel('dirrecion', $config);

$config = array();
$config['table'] = 'especificacion_rol';
$config['primary'] = 'cod_esp_rol';
$config['fields'] = array('nom_esp_rol', 'des_esp_rol', 'cod_rol');
ModelLoader::addModel('especificacion_rol', $config);

$config = array();
$config['table'] = 'examen';
$config['primary'] = 'cod_exa';
$config['fields'] = array('fec_sol_exa', 'fec_apl_exa', 'obs_exa', 'fec_ven', 'cod_tip_exa', 'cod_him');
ModelLoader::addModel('examen', $config);

$config = array();
$config['table'] = 'expediente';
$config['primary'] = 'cod_exp';
$config['fields'] = array('tip_san_exp', 'ale_exp', 'pad_exp', 'cod_usu');
ModelLoader::addModel('expediente', $config);

$config = array();
$config['table'] = 'factura';
$config['primary'] = 'cod_fact';
$config['fields'] = array('mon_med_fac', 'mon_tot', 'cod_tic', 'cod_usu', 'cod_tip');
ModelLoader::addModel('factura', $config);

$config = array();
$config['table'] = 'feriados';
$config['primary'] = 'cod_fer';
$config['fields'] = array('fec_ini_fer', 'fec_fin_fer');
ModelLoader::addModel('feriados', $config);

$config = array();
$config['table'] = 'grupo';
$config['primary'] = 'cod_gru';
$config['fields'] = array('nom_gru', 'des_gru');
ModelLoader::addModel('grupo', $config);

$config = array();
$config['table'] = 'historial_medico';
$config['primary'] = 'cod_him';
$config['fields'] = array('pes_him', 'fec_hor_him', 'tem_him', 'ten_him', 'pul_him', 'dia_him', 'can_exa', 'cod_exp', 'cod_tic', 'cod_cit');
ModelLoader::addModel('historial_medico', $config);

$config = array();
$config['table'] = 'historial_movimiento_medicamentos';
$config['primary'] = 'cod_hmm';
$config['fields'] = array('sen_hmm', 'fec_hmm', 'can_hmm', 'pro_hmm', 'val_hmm', 'cod_tra', 'cod_med', 'cod_usu', 'cod_fact');
ModelLoader::addModel('historial_movimiento_medicamentos', $config);

$config = array();
$config['table'] = 'horarios';
$config['primary'] = 'cod_hor';
$config['fields'] = array('fec_hor', 'hor_ini_hor', 'hor_fin_hor');
ModelLoader::addModel('horarios', $config);

$config = array();
$config['table'] = 'horaro_no_disponible_doctor';
$config['primary'] = 'cod_hnd';
$config['fields'] = array('fec_hnd', 'hor_ini_hnd', 'hor_fin_hnd', 'mot_hnd', 'cod_usu');
ModelLoader::addModel('horaro_no_disponible_doctor', $config);

$config = array();
$config['table'] = 'inventario_medicamento';
$config['primary'] = 'cod_med';
$config['fields'] = array('via_adm_med', 'nom_med', 'pres_med', 'exi_med', 'mon_med', 'prec_med', 'cod_sub');
ModelLoader::addModel('inventario_medicamento', $config);

$config = array();
$config['table'] = 'menu';
$config['primary'] = 'cod_men';
$config['fields'] = array('nom_men', 'url_men', 'cod_per');
ModelLoader::addModel('menu', $config);

$config = array();
$config['table'] = 'municipio';
$config['primary'] = 'cod_mun';
$config['fields'] = array('nom_mun', 'cod_dep');
ModelLoader::addModel('municipio', $config);

$config = array();
$config['table'] = 'permiso';
$config['primary'] = 'cod_per';
$config['fields'] = array('nom_per');
ModelLoader::addModel('permiso', $config);

$config = array();
$config['table'] = 'permiso_usuario';
$config['primary'] = 'cod_per_usu';
$config['fields'] = array('cod_per','cod_usu');
ModelLoader::addModel('permiso_usuario', $config);

$config = array();
$config['table'] = 'registro_modifiaciones_expedientes';
$config['primary'] = 'cod_rme';
$config['fields'] = array('fec_hor_rme','acc_rme', 'cod_exp', 'cod_usu');
ModelLoader::addModel('registro_modifiaciones_expedientes', $config);

$config = array();
$config['table'] = 'rol';
$config['primary'] = 'cod_rol';
$config['fields'] = array('nom_rol','des_rol');
ModelLoader::addModel('rol', $config);

$config = array();
$config['table'] = 'subgrupo';
$config['primary'] = 'cod_sub';
$config['fields'] = array('nom_sub','des_sub', 'cod_gru');
ModelLoader::addModel('subgrupo', $config);

$config = array();
$config['table'] = 'tipo_consulta';
$config['primary'] = 'cod_tic';
$config['fields'] = array('tip_tic','cos_tic');
ModelLoader::addModel('tipo_consulta', $config);

$config = array();
$config['table'] = 'tipo_examen';
$config['primary'] = 'cod_tip_exa';
$config['fields'] = array('nom_tip_exa', 'des_tip_exa');
ModelLoader::addModel('tipo_examen', $config);

$config = array();
$config['table'] = 'tipo_pago';
$config['primary'] = 'cod_tip';
$config['fields'] = array('nom_tip');
ModelLoader::addModel('tipo_pago', $config);

$config = array();
$config['table'] = 'tipo_tratamiento';
$config['primary'] = 'cod_tit';
$config['fields'] = array('nom_tit', 'des_tit');
ModelLoader::addModel('tipo_tratamiento', $config);

$config = array();
$config['table'] = 'tratamiento';
$config['primary'] = 'cod_tra';
$config['fields'] = array('fec_ini_tra', 'obs_tra', 'fec_fin_tra', 'cod_him', 'cod_tit');
ModelLoader::addModel('tratamiento', $config);

$config = array();
$config['table'] = 'usuario';
$config['primary'] = 'cod_usu';
$config['fields'] = array('nom1_usu', 'nom2_usu', 'nom3_usu', 'ape1_usu', 'ape2_usu', 'ape3_usu', 'fec_nac_usu', 'gen_usu', 'tel_cas_usu', 'tel_cel_usu', 'cor_usu', 'usu_usu', 'con_usu', 'jun_med_usu', 'fec_ins_usu', 'tip_doc_ide_usu', 'num_ide_usu', 'cod_esp_rol');
$config['relations'] = array('cita.cod_usu');
ModelLoader::addModel('usuario', $config);
