<?php // NOTE: El BaseManager es encargado de los preocesos del sistema como POST PUT y DELETE y obtener todos los elementos ?>
<?php
require_once '../moondragon4.0/moondragon.manager.php';
require_once '../moondragon4.0/moondragon.caller.php';

// NOTE: inmportamos los modelos

include 'models.php';

abstract class BaseManager extends JSONManager
{
	protected $model;

	public function __construct() {
		parent::__construct();
		//$model = new Model(Database::get);
	}

	public function index() {
		echo 'Control de metodos para JavaScript';
	}
	// NOTE: Funcion para obtener todos los elemnetos de la base de datos
	public function getall() {
		try {
			$result = $this->model->read();
			$data = array();
			foreach($result as $row) {
				$data[] = $row;
			}
			echo json_encode($data);
		}
		catch(DatabaseException $e) {
			$this->failure($e);
		}
	}
	// NOTE: funcion para obtener un solo elemento en la base de datos
	public function get() {
		try {
			$id = Request::getGET('id');
			$data = $this->model->getData($id);
			echo json_encode($data);
		}
		catch(DatabaseException $e) {
			$this->failure($e);
		}
		catch(RequestException $e) {
			$this->failure($e);
		}
	}
	// NOTE: Funcion para insertar en la base de datps
	public function insert() {
		try {
			$data = $this->get_data();
			$id = $this->model->create($data);
			$this->success(array('id' => $id));
		}
		catch(RequestException $e) {
			$this->failure($e);
		}
		catch(DatabaseException $e) {
			$this->failure($e);
		}
	}
	// NOTE: Funcion para hacer un update a la base de datos
	public function update() {
		try {
			$id = Request::getGET('id');
			$data = $this->get_data();
			$this->model->update($id, $data);
			$this->success();
		}
		catch(RequestException $e) {
			$this->failure($e);
		}
		catch(DatabaseException $e) {
			$this->failure($e);
		}
	}

	public function update_res() {
		try {
			$id = Request::getGET('cod_res');
			$data = $this->get_data();
			$this->model->update($id, $data);
			$this->success();
		}
		catch(RequestException $e) {
			$this->failure($e);
		}
		catch(DatabaseException $e) {
			$this->failure($e);
		}
	}
	// NOTE: funcion para eliminar un elemento a la  base de datps
	public function delete() {
		try {
			$id = Request::getPOST('id');
			$this->model->delete($id);
		}
		catch(RequestException $e) {
			$this->failure($e);
		}
		catch(DatabaseException $e) {
			$this->failure($e);
		}
	}
	protected function get_data() {
		return $this->model->getDataset();
	}
	// NOTE: Funcion de suvccess
	protected function success($info = array()) {
		$data['success'] = true;
		$data['info'] = $info;
		echo $this->formatResponse($data);
	}
// NOTE: Funcion de failure
	protected function failure($e = 'Unknown Error') {
		$data['success'] = false;
		$data['error'] = (string) $e;
 		echo $this->formatResponse($data);
	}
}
