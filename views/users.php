<?php

include "../database/BaseManager.php";

class Usuarios extends BaseManager{

	public function index(){}


		public function __construct() {
			parent::__construct();
			$this->model = ModelLoader::getModel('usuario');
		}

	public function getOne(){
		$db = Database::getManager();
		$user_id = Request::getGET('id');

		$array = array();

		// NOTE: Obteniendo el usuario
		$user = ModelLoader::getModel('usuario');

		$reader = $user->getReader();
		$reader->addWhere('cod_usu', $user_id);
		$result = $reader->getRows();

		foreach($result as $r) {
			$array['usuario'] = $r;
		}

		// NOTE: Para obtener el tipo de rol que tiene

		$id_rol = $array['usuario']->cod_esp_rol;

		$rol = ModelLoader::getModel('especificacion_rol');

		$reader = $rol->getReader();
		$reader->addWhere('cod_esp_rol', $id_rol);
		$result = $reader->getRows();

		foreach ($result as $rol) {
			$array['rol'] = $rol;
		}

		// NOTE: Obteniendo el expediente
		$expediente = ModelLoader::getModel('expediente');

		$reader = $expediente->getReader();
		$reader->addWhere('cod_usu', $user_id);
		$result = $reader->getRows();

		foreach($result as $r) {
			$array['expediente'] = $r;
		}


		// NOTE: Obteniendo los datos de la cita
		$control = ModelLoader::getModel('cita');

		$reader = $control->getReader();
		$reader->addWhere('cod_usu', $user_id);
		$result = $reader->getRows();

		$controles = array();

		foreach ($result as $cita) {
			array_push($controles, $cita);
		}

		$array['citas'] = $controles;

		echo json_encode($array);

	}

	public function get_data() {
		$dataset = $this->model->getDataset();
		$dataset->nom1_usu = Request::getGET('nom1_usu');
		$dataset->ape1_usu = Request::getGET('ape1_usu');
		$dataset->fec_nac_usu = Request::getGET('fec_nac_usu');
		$dataset->gen_usu = Request::getGET('gen_usu');
		$dataset->con_usu = Request::getGET('con_usu');
		$dataset->fec_ins_usu = Request::getGET('fec_ins_usu');
		$dataset->usu_usu = Request::getGET('usu_usu');
		$dataset->cod_esp_rol= Request::getGET('cod_esp_rol');
		$dataset->cor_usu= Request::getGET('cor_usu');
		return $dataset;
	}
}

MoonDragon::run(new Usuarios());
?>
