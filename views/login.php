<?php // NOTE: Modulo para el login de la aplicacion ?>
<?php
// NOTE: Agregamos todas las dependencias del framework para que funcionen las vistas
include '../moondragon4.0/moondragon.database.php';
include '../connection/conexion.php';
include '../moondragon4.0/moondragon.session.php';
include '../moondragon4.0/moondragon.manager.php';

// NOTE: Creamos una nueva clase para el login
class LoginManager extends Manager {
	public function index() {}

	// NOTE: Creamos una funcion publica llamada login que es la que mandamos a llamar para que pueda ser usada. (Recibe como parametros el usuario y la contrasena por el metodo GET)
	public function login() {
		// NOTE: creamos una variable que contiene la base de datos
		$db = Database::getManager();
		// NOTE: Recibimos por el metodo GET los parametros user y password
		$usuario = Request::getGET('user');
		$password = Request::getGET('password');

		// NOTE: Hacemos una query directa a la base de datos para obtener los usuarios que sean iguales a los usuarios recibidos en el parametro (en teoria deberia de ser solo uno)
       	$existe = $db->query("SELECT * FROM  usuario WHERE usu_usu =  '".$usuario."'");

		// NOTE: verificamos si existe el usuario en la base de datos
		if ($existe->numRows()== 0)
		{
			// NOTE: Si no existe retornamos el valor 0
			echo '0';
		}
		else
		{
			// NOTE: Si acaso existe el usuario entonces obtenemos el valor de la password de el usuario de la base de datos
			$passwordbd = $existe->getResult('con_usu');
			$passwordform = ($password);
			// NOTE: comprando si los password hacen match
			if ($passwordform == $passwordbd)
			{
				// NOTE: Si las passwors hacen match entonces se crea un array que retorna en status de la peticion y el id del usuario
				$a = array ('status' => True, 'user' => $existe->getResult('cod_usu'));
				echo json_encode($a);
			}
			else
			{
				// NOTE: Se retorna el status de la peticion false si las contrasenas no hacen match
				$a = array ('status' => False);
				echo json_encode($a);
			}
		}
	}
}
MoonDragon::run(new LoginManager());
