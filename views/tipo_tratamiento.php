<?php

include "../database/BaseManager.php";

class TipoTratamiento extends BaseManager
{
	public function __construct() {
		parent::__construct();
		$this->model = ModelLoader::getModel('tipo_tratamiento');
	}
	public function get_data() {
		$dataset = $this->model->getDataset();
		$dataset->cod_tic = Request::getGET('cod_tit');
		$dataset->tip_tic = Request::getGET('nom_tit');
		$dataset->cos_tic = Request::getGET('des_tit');
		return $dataset;
	}
}

MoonDragon::run(new TipoTratamiento());
