<?php

include "../database/BaseManager.php";

class Historial_Medico extends BaseManager
{
	public function __construct() {
		parent::__construct();
		$this->model = ModelLoader::getModel('historial_medico');
	}
	public function get_data() {
		$dataset = $this->model->getDataset();
		$dataset->tem_him = Request::getGET('tem_him');
		$dataset->pes_him = Request::getGET('pes_him');
		$dataset->ten_him = Request::getGET('ten_him');
		$dataset->pul_him= Request::getGET('pul_him');
        $dataset->can_exa= Request::getGET('can_exa');
        $dataset->cod_tic= Request::getGET('cod_tic');
        $dataset->cod_cit= Request::getGET('cod_cit');
        $dataset->cod_exp= Request::getGET('cod_exp');
		return $dataset;
	}
}

MoonDragon::run(new Historial_Medico());
