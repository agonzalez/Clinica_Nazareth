<?php
$modo = $_POST['modo'];
if ($modo == 'contacto')
{

	$correo= $_POST['correo'];
	$cita = $_POST['cita'];
	$fecha = $_POST['fecha'];
	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];


	// codigo html del correo
	$content = "<html xmlns='http://www.w3.org/1999/xhtml'>
	<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	</head>
	<body>
	Querido, $nombre $apellido<br />
	Este es un recordatorio para su cita: $cita <br />
	programada para: $fecha<br />
	Muchas Gracias.
	</body>
	</html>";
	// fin codigo del correo
	require_once('class.phpmailer.php');
	$mail = new PHPMailer();
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->SMTPAuth   = true;                  // enable SMTP authentication
	//$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
	$mail->Host       = "mail.mailsystem.me";      // sets GMAIL as the SMTP server
	$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
	$mail->Username   = "noreply@mailsystem.me";  // GMAIL username
	$mail->Password   = "mailsystemadmin";            // GMAIL password
	$mail->SMTPDebug = 1;
	$mail->From = 'noreply@mailsystem.me';		// Correo de remitente
	$mail->FromName = 'Contacto | Cliente';				//Nombre de remitente
	$mail->Subject    = UTF8_decode("Confirmacion de cita"); //Asunto del correo
	$mail->AltBody    = strip_tags(str_replace( '<br/>', "\n", $content));
	$mail->MsgHTML(utf8_decode($content));
	$mail->AddAddress($correo, "Recordatorio de Cita"); //Destinatario
	$mail->clearReplyTos();
	$mail->AddReplyTo('noreply@mailsystem.me',"No-Reply");
	if(!$mail->Send())
	{
		return 'Ocurrio un error, intenta más tarde';
	}
	else
	{
		return 'Mensaje enviado correctamente';
	}
}
else
{
	echo 'Tu no deberias estar aqui';
}
?>
