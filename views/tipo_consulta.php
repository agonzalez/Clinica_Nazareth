<?php

include "../database/BaseManager.php";

class TipoConsultas extends BaseManager
{
	public function __construct() {
		parent::__construct();
		$this->model = ModelLoader::getModel('tipo_consulta');
	}
	public function get_data() {
		$dataset = $this->model->getDataset();
		$dataset->cod_tic = Request::getGET('cod_tic');
		$dataset->tip_tic = Request::getGET('tip_tic');
		$dataset->cos_tic = Request::getGET('cos_tic');
		return $dataset;
	}
}

MoonDragon::run(new TipoConsultas());
