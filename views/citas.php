<?php // NOTE: Vista de citas para obtener las citas especificas y sus cosas ?>
<?php

include "../database/BaseManager.php";

class Citas extends BaseManager
{
	public function index(){}


		public function __construct() {
			parent::__construct();
			$this->model = ModelLoader::getModel('cita');
		}

	public function validate(){
		$db = Database::getManager();

		$date = Request::getGET('date');

		$existe = $db->query("SELECT * FROM  cita WHERE ini_cit =  '".$date."'");

		if ($existe->numRows() == 0)
		{
			echo '0';
		}
		else
		{
			echo '1';
		}



	}
	// NOTE: Funcion para obtener una sola cita
	public function getOne(){
		// NOTE: Obtenemos la base de datos
		$db = Database::getManager();
		// NOTE: Obtenemos el id del usuario
		$cita_id = Request::getGET('id');
		// NOTE: Creamos un areglo vacio
		$array = array();

		// NOTE: Obteniendo el usuario
		$cita = ModelLoader::getModel('cita');

		$reader = $cita->getReader();
		$reader->addWhere('cod_cit', $cita_id);
		$result = $reader->getRows();

		foreach($result as $r) {
			$array['cita'] = $r;
		}

		// NOTE: Obteniendo el usuario
		$user = ModelLoader::getModel('usuario');

		$reader = $user->getReader();
		$reader->addWhere('cod_usu', $array['cita']->cod_usu);
		$result = $reader->getRows();

		foreach($result as $r) {
			$array['usuario'] = $r;
		}

		$historial = ModelLoader::getModel('historial_medico');

		$reader = $historial->getReader();
		$reader->addWhere('cod_cit', $cita_id);
		$result = $reader->getRows();

		foreach($result as $r) {
			$array['historial'] = $r;
		}

		$tratamiento = ModelLoader::getModel('tipo_consulta');

		$reader = $tratamiento->getReader();
		$reader->addWhere('cod_tic', $array['historial']->cod_tic);
		$result = $reader->getRows();

		foreach($result as $r) {
			$array['costo'] = $r;
		}

		$tipo_tratamiento = ModelLoader::getModel('tratamiento');

		echo json_encode($array);

	}

	public function get_data() {
		$dataset = $this->model->getDataset();
		$dataset->fin_cit = Request::getGET('fin_cit');
		$dataset->ini_cit = Request::getGET('ini_cit');
		$dataset->mot_cit = Request::getGET('mot_cit');
		$dataset->est_cit = Request::getGET('est_cit');
		$dataset->cod_usu= Request::getGET('cod_usu');
		return $dataset;
	}
}

MoonDragon::run(new Citas());
