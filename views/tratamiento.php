<?php

include "../database/BaseManager.php";

class Tratamiento extends BaseManager
{
	public function __construct() {
		parent::__construct();
		$this->model = ModelLoader::getModel('tratamiento');
	}

	public function getOne(){
		$db = Database::getManager();
		$tratamiento_id = Request::getGET('id');

		$array = array();

		// NOTE: Obteniendo el usuario
		$cita = ModelLoader::getModel('tratamiento');

		$reader = $cita->getReader();
		$reader->addWhere('cod_him', $historial_id);
		$result = $reader->getRows();

		foreach($result as $r) {
			$array['tratamiento'] = $r;
		}

		echo json_encode($array);

	}

	public function get_data() {
		$dataset = $this->model->getDataset();
		$dataset->fec_ini_tra = Request::getGET('fec_ini_tra');
		$dataset->obs_tra = Request::getGET('obs_tra');
		$dataset->cod_him= Request::getGET('cod_him');
        $dataset->cod_tit= Request::getGET('cod_tit');
		return $dataset;
	}
}

MoonDragon::run(new Tratamiento());
